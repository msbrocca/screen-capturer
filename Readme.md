
# LinC ScreenGrabber

## Important Notes

Here you will find some useful information about required libraries, that are not easy to install (at least in Ubuntu and MAC).
This app was tested/used in Ubuntu and MAC.
This tool born due to the impossibility of use Jing on Ubuntu.

Currently, we are just uploading images to three different servers

* imgur.com
* freeimagehosting.net
* clip2net.com


I contacted techsmith forum to see if there is an API to upload images to ScreenCast.com, but I didn't receive any
response yet.

#### Python ScreenShot

https://github.com/ponty/pyscreenshot

python-screenshot requires one of the following backends:

* scrot
* ImageMagick
* PyGTK
* PIL (only on windows)
* PyQt4
* wxPython

This software is based on *ImageMagick*

#### PyCURL

To 'install' pycurl inside virtualenv, follow this steps

```
http://stackoverflow.com/questions/11441546/how-i-can-make-apt-get-install-to-my-virtualenv
```

If you are not working with virtualenv, just make:

```
sudo apt-get install python-pycurl
```

#### What do you need to run recordscreen.py?

To be able to capture videos, please run the follow command. So far, video capture is just supported on Linux systems
```
sudo apt-get install wget libav-tools ffmpeg libavc1394-0 libavformat-extra-53 libavfilter2 libavutil-extra-51 mencoder libavahi-common-data
```

## How to use it

Once you have cloned it and have installed all the requirements (requirements.txt and all needed soft/libs listed here) run the app as follow:

1.- Run the script:
```
~/screen-capturer/screencapture$ python capturer.py
```

This will open this window: http://i.imgur.com/RokFOl9.png

#### Screen Capture

2.1.1- Press the "Start Capturing" button. It will lock all the screen with a frame that will allows you to delimit the area
you want to capture to share/save. You can cancel/close this frame by clicking the right mouse button.

2.1.2.- Select the area to share/save left clicking in the start point and then releasign the mouse left button in the end point.
Remember that you can cancel all by clicking right mouse button

2.1.3.- Once you finished with point 3, you'll see an screen with the area you caught, with the buttons to save, share and quit.
Something like this: http://i.imgur.com/R2pffMU.png (creator and girlfriend in Humahuaca - Jujuy - Argentina :D)

2.1.3.1.- Save! will allows yoy to save the png file to disk

2.1.3.2.- Share! will upload the image to http://imgur.com/ (So far that's the only option. The idea is that Share! allows you
to upload images to more servers)

2.1.3.3.- Quit! will close that window (but not main window!, so you'll be able to continue capturing)

#### Video Capture

2.2.1- Press the "Start Video (beta)" button. First of all you'll see a dialog message that explain how to use this functionality (http://i.imgur.com/BgA1LPC.png). Once you press the "Ok" button of the dialog the screen will be locked with a frame that will allows you to delimit the area
you want to record. You can cancel/close this frame by clicking the right mouse button.

2.2.2.- Select the area to record left clicking in the start point and then releasign the mouse left button in the end point.
Remember that you can cancel all by clicking right mouse button.

2.2.3.- Once you finished with point 3, you'll see an screen with the video record options. The option you choose will record the area you selected: http://i.imgur.com/rL4F8tq.png

2.2.4.- If you started a video capture, you can stop it pressing STOP or QUIT options. Both options will stop the video capture and will save a video file inside main folder of LinC. You will see a message dialog informing that.

# Donation

If this tool was useful for you and you think it deserves a donation from you, you can help us following this link
and donating what you think is appropriate:

[DONATE](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=6MVA3H8Z95ETE&lc=AR&item_name=LinC%20ScreenGrabber&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)
