__author__ = 'maxi'
from subprocess import Popen, PIPE
from uimge import uimge

supported_hosts_map = {
    'imgur': 'ig_imgur',
    'clip2net': 'c2n_clip2net',
    'freeimagehosting.net': 'fih_fileimagehosting'
}


def share_image(filename, server):
    image = uimge.Uimge()
    image.set_host(uimge.Hosts.hosts_dict.get(supported_hosts_map.get(server, 'ig_imgur')))
    image.upload(filename)
    return image.get_urls()