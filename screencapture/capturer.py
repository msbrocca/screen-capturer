__author__ = 'maxi'
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import os
import uuid
import logging
import platform
import time


import pyscreenshot as ImageGrab
from PIL import ImageTk
import pyperclip
import recordscreen


class MainWindow(tk.Tk):
    def __init__(self, ):
        tk.Tk.__init__(self)
        self.title('LinC ScreenGrabber v0.1b')
        self.frame = tk.Frame(self)
        self.button1 = tk.Button(self.frame, text='Start Capturing', width=35,
                                 command=self.new_window)
        if 'Linux' in platform.platform():
            self.record_button = tk.Button(self.frame, text='Start Video (beta)', width=35, command=self.video_window)
            self.record_button.pack()
        self.button1.pack()
        self.frame.pack()

    def new_window(self):
        CapturerWindow()

    def video_window(self):
        tkMessageBox.showinfo("Info", "Now you will have to delimit the area you want to capture. Then, the options will be displayed. Once you choice one of the record options, after 3 seconds the video will start capturing the area you choose (unfortunately, there no indication of that area... is in your mind :S) ")
        VideoCapturerWindow()



class CapturerWindow(tk.Toplevel):
    def __init__(self):
        self.point_one = None
        self.point_two = None

        tk.Toplevel.__init__(self)
        self.overrideredirect(True)

        self.wait_visibility(self)
        self.attributes('-topmost', True)
        self.attributes('-alpha', 0.1)

        maxW = self.winfo_screenwidth()
        maxH = self.winfo_screenheight()
        w = tk.Canvas(self, width=maxW, height=maxH)
        w.config(cursor='cross')
        w.pack(expand=tk.YES, fill=tk.BOTH)
        self.geometry("{0}x{1}+0+0".format(maxW, maxH))

        # Binding Events
        self.bind('<Button-3>', self.exit_code)
        self.bind("<ButtonPress-1>", self.press)
        self.bind("<ButtonRelease-1>", self.release)

        # This will creates a rectangle on the delimitation area
        rect = RectTracker(w)
        rect.autodraw(fill="", width=2)

    def exit_code(self, event):
        self.destroy()

    def press(self, event):
        if self.point_one is None:
            logging.debug(
                "Press Point: X={0} - Y={1}".format(event.x_root, event.y_root))
            self.point_one = (event.x_root, event.y_root)

    def release(self, event):
        if self.point_two is None:
            logging.debug("Relsea Point: X={0} - Y={1}".format(event.x_root,
                                                               event.y_root))
            self.point_two = (event.x_root, event.y_root)


        try:
            ## Check box
            x = self.point_one[0]
            y = self.point_one[1]
            x1 = self.point_two[0]
            y1 = self.point_two[1]

            x2 = x if x < x1 else x1
            y2 = y if y < y1 else y1

            width = abs(x - x1)
            height = abs(y - y1)

            im = ImageGrab.grab(bbox=(x2,
                                      y2,
                                      width,
                                      height))

            self.destroy()
            VideoOptionsWindow(
                width=width,
                height=height,
                x=x2,
                y=y2,
                im=im)
        except Exception as ex:
            self.destroy()
            tkMessageBox.showerror('Error Backend', ex.message)


class VideoCapturerWindow(tk.Toplevel):
    def __init__(self):
        self.point_one = None
        self.point_two = None

        tk.Toplevel.__init__(self)
        self.overrideredirect(True)

        self.wait_visibility(self)
        self.attributes('-topmost', True)
        self.attributes('-alpha', 0.1)

        maxW = self.winfo_screenwidth()
        maxH = self.winfo_screenheight()
        w = tk.Canvas(self, width=maxW, height=maxH)
        w.config(cursor='cross')
        w.pack(expand=tk.YES, fill=tk.BOTH)
        self.geometry("{0}x{1}+0+0".format(maxW, maxH))

        # Binding Events
        self.bind('<Button-3>', self.exit_code)
        self.bind("<ButtonPress-1>", self.press)
        self.bind("<ButtonRelease-1>", self.release)

        # This will creates a rectangle on the delimitation area
        rect = RectTracker(w)
        rect.autodraw(fill="", width=2)

    def exit_code(self, event):
        self.destroy()


    def press(self, event):
        if self.point_one is None:
            logging.debug(
                "Press Point: X={0} - Y={1}".format(event.x_root, event.y_root))
            self.point_one = (event.x_root, event.y_root)

    def release(self, event):
        if self.point_two is None:
            logging.debug("Relsea Point: X={0} - Y={1}".format(event.x_root,
                                                               event.y_root))
            self.point_two = (event.x_root, event.y_root)


        try:
            ## Check box
            x = self.point_one[0]
            y = self.point_one[1]
            x1 = self.point_two[0]
            y1 = self.point_two[1]

            x2 = x if x < x1 else x1
            y2 = y if y < y1 else y1

            width = abs(x - x1)
            height = abs(y - y1)

            self.destroy()
            VideoOptionsWindow(
                width=width,
                height=height,
                x=x2,
                y=y2)
        except Exception as ex:
            self.destroy()
            tkMessageBox.showerror('Error Backend', ex.message)


class VideoOptionsWindow(tk.Toplevel):
    def __init__(self, width, height, x, y):

        tk.Toplevel.__init__(self)
        self.title("LinC ScreenGrabber v0.1b")
        self.geometry("{0}x{1}+{2}+{3}".format(400, 10, x-100, y-100))

        #self.overrideredirect(True)

        #self.wait_visibility(self)
        #self.attributes('-topmost', True)
        #self.attributes('-alpha', 0.1)

        menubar = tk.Menu(self)
        menubar.add_command(label="Record with Sound!", command=lambda: self.record(x, y, width, height))
        menubar.add_command(label="Record without Sound!", command=lambda: self.record(x, y, width, height, False))
        menubar.add_command(label="STOP", command=lambda: self.stop())
        menubar.add_command(label="Quit!", command=lambda: self.exit_record())
        self.config(menu=menubar)

        self.record_process = None

    def record(self, x, y, w, h, audio=True):
        time.sleep(3)
        import sys
        sys.stdout.write('\a')
        sys.stdout.flush()
        self.record_process = recordscreen.start_record(x, y, w, h, audio)

    def stop(self):
        if self.record_process is not None:
            self.record_process.kill()
            self.record_process = None
            tkMessageBox.showinfo("Info", "New video was saved inside LinC main folder")

    def exit_record(self):
        self.stop()
        self.destroy()


class CapturerWindow(tk.Toplevel):
    def __init__(self):
        self.point_one = None
        self.point_two = None

        tk.Toplevel.__init__(self)
        self.overrideredirect(True)

        self.wait_visibility(self)
        self.attributes('-topmost', True)
        self.attributes('-alpha', 0.1)

        maxW = self.winfo_screenwidth()
        maxH = self.winfo_screenheight()
        w = tk.Canvas(self, width=maxW, height=maxH)
        w.config(cursor='cross')
        w.pack(expand=tk.YES, fill=tk.BOTH)
        self.geometry("{0}x{1}+0+0".format(maxW, maxH))

        # Binding Events
        self.bind('<Button-3>', self.exit_code)
        self.bind("<ButtonPress-1>", self.press)
        self.bind("<ButtonRelease-1>", self.release)

        # This will creates a rectangle on the delimitation area
        rect = RectTracker(w)
        rect.autodraw(fill="", width=2)

    def exit_code(self, event):
        self.destroy()

    def press(self, event):
        if self.point_one is None:
            logging.debug(
                "Press Point: X={0} - Y={1}".format(event.x_root, event.y_root))
            self.point_one = (event.x_root, event.y_root)

    def release(self, event):
        if self.point_two is None:
            logging.debug("Relsea Point: X={0} - Y={1}".format(event.x_root,
                                                               event.y_root))
            self.point_two = (event.x_root, event.y_root)


        try:
            ## Check box
            x = self.point_one[0]
            y = self.point_one[1]
            x1 = self.point_two[0]
            y1 = self.point_two[1]

            x2 = x if x < x1 else x1
            y2 = y if y < y1 else y1

            width = abs(x - x1)
            height = abs(y - y1)

            im = ImageGrab.grab(bbox=(x2,
                                      y2,
                                      width,
                                      height))

            self.destroy()
            OptionsWindow(
                width=width,
                height=height,
                x=x2,
                y=y2,
                im=im)
        except Exception as ex:
            self.destroy()
            tkMessageBox.showerror('Error Backend', ex.message)


class OptionsWindow(tk.Toplevel):
    def __init__(self, width, height, x, y, im):
        self.im = im

        tk.Toplevel.__init__(self)
        self.title("LinC ScreenGrabber v0.1b")
        self.geometry("{0}x{1}+{2}+{3}".format(width, height, x, y))
        menubar = tk.Menu(self)
        menubar.add_command(label="Save!", command=lambda: self.save(self.im))
        # Share - Servers options
        share_options = tk.Menu(menubar, tearoff=False)
        share_options.add_command(label="imgur",
                                  command=lambda: self.share(self.im,
                                                             'imgur'))
        share_options.add_command(label="clip2net",
                                  command=lambda: self.share(self.im,
                                                             'clip2net'))
        share_options.add_command(label="freeimagehosting.net",
                                  command=lambda: self.share(self.im,
                                                             'freeimagehosting.net'))

        menubar.add_cascade(label="Share Servers", menu=share_options)
        menubar.add_command(label="Quit!", command=self.destroy)
        self.config(menu=menubar)

        photo = ImageTk.PhotoImage(im)
        self.label = tk.Label(self, image=photo)
        self.label.image = photo
        self.label.pack()

    def share(self, im, server_name):
        import share

        result = tkMessageBox.askquestion("Info", "Image will be uploaded to "
                                                  "{0}. Do you want to "
                                                  "continue?".format(
            server_name), icon=tkMessageBox.QUESTION)
        if result == 'yes':
            folder = os.path.join(os.path.dirname(__file__), 'images')
            filename = os.path.join(folder, str(uuid.uuid4()).replace('-', '') +
                                            '.png')
            im.save(filename)
            urls = share.share_image(filename, server_name)
            big = urls[0]
            #TODO this could be used in the future
            thumb = urls[1]
            pyperclip.copy(big)
            tkMessageBox.showinfo("Information",
                                  "Your image URL is: " + big + ". "
                                                                "It "
                                                                "was "
                                                                "coppied to clipboard.")

    def save(self, im):
        options = dict()
        options['filetypes'] = [('all files', '.*'), ('image files', '.png')]
        options['initialfile'] = 'capture.png'
        options['parent'] = self
        filename = tkFileDialog.asksaveasfile('w', **options)
        if filename:
            im.save(filename)
            tkMessageBox.showinfo("Save as Info", "Image Saved!!")
            return True


class RectTracker:
    def __init__(self, canvas):
        self.canvas = canvas
        self.item = None
        self.last_selection = None
        self.start = None

    def draw(self, start, end, **opts):
        """Draw the rectangle"""
        return self.canvas.create_rectangle(*(list(start) + list(end)), **opts)

    def autodraw(self, **opts):
        """Setup automatic drawing; supports command option"""
        self.canvas.bind("<Button-1>", self.__update, '+')
        self.canvas.bind("<B1-Motion>", self.__update, '+')
        self.canvas.bind("<ButtonRelease-1>", self.__stop, '+')

        self._command = opts.pop('command', lambda *args: None)
        self.rectopts = opts

    def __update(self, event):
        if not self.start:
            self.start = [event.x, event.y]
            return

        if self.item is not None:
            self.canvas.delete(self.item)
        self.item = self.draw(self.start, (event.x, event.y), **self.rectopts)
        self._command(self.start, (event.x, event.y))

    def __stop(self, event):
        self.last_selection = (self.start[0], self.start[1],
                               event.x_root, event.y_root)
        self.start = None
        self.canvas.delete(self.item)
        self.item = None

def main():
    app = MainWindow()
    app.mainloop()


if __name__ == '__main__':
    main()