# -*- coding: utf-8 -*-
import base

class Host(base.BaseHost):
    short_key = 'fih'
    long_key = 'fileimagehosting'
    host = 'www.freeimagehosting.net'
    action = '%s/upload.php' % host
    form = {
        'submit': ''
    }

    def as_file(self, _file):
        return {'attached': _file}

    def as_url(self, _url):
        return { 'attached': _url }

    def postload(self ):
        _src = self.response.body
        url = 'http://www.freeimagehosting.net/'
        tmp = self.findall('value="http://www.freeimagehosting.net/\w*',  _src )[0]
        img_location = tmp.split(url)[1]
        self.img_url = url + img_location
        self.img_thumb_url = url + 't/' + img_location + '.jpg'

if __name__ == '__main__':
    h= Host()
    h.test_file()
    h.test_url()