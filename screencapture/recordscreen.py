#!/usr/bin/env python

""" A simple screen-capture utility.  Utilizes avconv with h264 support.
By default it captures the entire desktop.
"""

################################ LICENSE BLOCK ################################
# Copyright (c) 2011 Nathan Vegdahl
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
###############################################################################

# Easy-to-change defaults for users
DEFAULT_FPS = 15
DEFAULT_FILE_EXTENSION = ".mkv"
ACCEPTABLE_FILE_EXTENSIONS = [".avi", ".mp4", ".mov", ".mkv", ".ogv"]
DEFAULT_CAPTURE_AUDIO_DEVICE = "pulse"
DEFAULT_CAPTURE_DISPLAY_DEVICE = ":0.0"
DEFAULT_AUDIO_CODEC = "vorbis"
DEFAULT_VIDEO_CODEC = "h264_fast"

import sys
import glob
import subprocess
import re


PYTHON_3 = (sys.version_info[0] == 3)


# Optional packages
try:
    import Tkinter
    have_tk = True
except ImportError:
    have_tk = False

try:
    import multiprocessing
    have_multiproc = True
except ImportError:
    have_multiproc = False


# Video codec lines
vcodecs = {}
vcodecs["h264"] = ["-vcodec", "libx264", "-preset", "medium", "-cqp", "0"]
vcodecs["h264_fast"] = ["-vcodec", "libx264", "-preset", "ultrafast", "-g", "15", "-crf", "0", "-pix_fmt", "yuv444p"]
vcodecs["mpeg4"] = ["-vcodec", "mpeg4", "-qmax", "1", "-qmin", "1"]
#vcodecs["xvid"] = ["-vcodec", "libxvid", "-b", "40000kb"]
vcodecs["huffyuv"] = ["-vcodec", "huffyuv"]
vcodecs["vp8"] = ["-vcodec", "libvpx", "-qmax", "2", "-qmin", "1"]
vcodecs["theora"] = ["-vcodec", "libtheora", "-b", "40000kb"]
#vcodecs["dirac"] = ["-vcodec", "libschroedinger", "-b", "40000kb"]

# Audio codec lines
acodecs = {}
acodecs["pcm"] = ["-acodec", "pcm_s16le"]
#acodecs["flac"] = ["-acodec", "flac"]
acodecs["vorbis"] = ["-acodec", "libvorbis", "-ab", "320k"]
acodecs["mp3"] = ["-acodec", "libmp3lame", "-ab", "320k"]
acodecs["aac"] = ["-acodec", "libfaac", "-ab", "320k"]


def capture_line(fps, x, y, height, width, display_device, audio_device, video_codec, audio_codec, output_path):
    """ Returns the command line to capture video+audio, in a list form
        compatible with Popen.
    """
    threads = 2
    if have_multiproc:
        # Detect the number of threads we have available
        threads = multiprocessing.cpu_count()
    line = ["avconv",
            "-f", "alsa",
            "-ac", "2",
            "-i", str(audio_device),
            "-f", "x11grab",
            "-r", str(fps),
            "-s", "%dx%d" % (int(height), int(width)),
            "-i", display_device + "+" + str(x) + "," + str(y),
            "-vf", "crop=%d:%d:%d:%d" % (int(height), int(width), int(x), int(y))
    ]
    line += acodecs[audio_codec]
    line += vcodecs[video_codec]
    line += ["-threads", str(threads), str(output_path)]
    return line


def video_capture_line(fps, x, y, height, width, display_device, video_codec, output_path):
    """ Returns the command line to capture video (no audio), in a list form
        compatible with Popen.
    """
    threads = 2
    if have_multiproc:
        # Detect the number of threads we have available
        threads = multiprocessing.cpu_count()

    line = ["avconv",
            "-f", "x11grab",
            "-r", str(fps),
            "-s", "%dx%d" % (int(height), int(width)),
            "-i", display_device + "+" + str(x) + "," + str(y),
            "-vf", "crop=%d:%d:%d:%d" % (int(height), int(width), int(x), int(x))
    ]
    line += vcodecs[video_codec]
    line += ["-threads", str(threads), str(output_path)]
    return line


def audio_capture_line(audio_device, audio_codec, output_path):
    """ Returns the command line to capture audio (no video), in a list form
        compatible with Popen.
    """
    line = ["avconv",
            "-f", "alsa",
            "-ac", "2",
            "-i", str(audio_device)]
    line += acodecs[audio_codec]
    line += [str(output_path)]
    return line


def get_default_output_path():
    """ Creates a default output file path.
        Pattern: out_####.ext
    """
    filenames = glob.glob("out_????" + DEFAULT_FILE_EXTENSION)
    for i in range(1, 9999):
        name = "out_" + str(i).rjust(4,'0') + DEFAULT_FILE_EXTENSION
        tally = 0
        for f in filenames:
            if f == name:
                tally += 1
        if tally == 0:
            return name
    return "out_9999" + DEFAULT_FILE_EXTENSION


def start_record(x, y, w, h, with_audio=True):
    out_path = get_default_output_path()
    fps = DEFAULT_FPS

    mults = {"h264": 2, "h264_fast": 2, "mpeg4": 2, "dirac": 2, "xvid": 2, "theora": 8, "huffyuv": 2, "vp8": 1}
    w -= w % mults[DEFAULT_VIDEO_CODEC]
    h -= h % mults[DEFAULT_VIDEO_CODEC]

    # Capture!
    if with_audio:
        return subprocess.Popen(capture_line(fps, x, y, w, h, DEFAULT_CAPTURE_DISPLAY_DEVICE, DEFAULT_CAPTURE_AUDIO_DEVICE, DEFAULT_VIDEO_CODEC, DEFAULT_AUDIO_CODEC, out_path))
    else:
        return subprocess.Popen(video_capture_line(fps, x, y, w, h, DEFAULT_CAPTURE_DISPLAY_DEVICE, DEFAULT_VIDEO_CODEC, out_path))